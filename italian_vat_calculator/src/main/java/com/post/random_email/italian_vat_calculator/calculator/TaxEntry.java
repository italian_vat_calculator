/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.italian_vat_calculator.calculator;

/**
 *
 * @author salvix
 */
public class TaxEntry {
	
	public double amount;
	public String description;
	
	
	public String naiveRender()
	{
		return description +" : "+Calculator.formatDecimal(amount);
	}
}
