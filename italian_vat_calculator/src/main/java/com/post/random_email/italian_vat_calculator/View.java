package com.post.random_email.italian_vat_calculator;

import com.post.random_email.italian_vat_calculator.calculator.Calculator;
import com.post.random_email.italian_vat_calculator.calculator.TaxEntry;
import java.util.List;


public class View {
	
	public static String layout(String title, String content)
	{
		String docType = "<!DOCTYPE html><html>";
		String titleTag = "<title>"+title+"</title>";
		String meta =  "<meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
		String style = css("https://cdnjs.cloudflare.com/ajax/libs/kube/6.5.2/css/kube.min.css");
//		String js = js("");


		
		
		String innerContent = "<h1>"+title+"</h1>"+content; 
		
		String actualContent = "<div class=\"row\">";
		actualContent += "<div class=\"col col-1\"></div>";
		actualContent += "<div class=\"col col-10\">"+innerContent+"</div>";
		actualContent += "<div class=\"col col-1\"></div>";
		actualContent += "</div>";
		
		String body = "<body>"+actualContent+"</body></html>";
		String fullHead = docType+"<head>"+titleTag+meta+style+"</head>"; 
		return fullHead+body;
	}
	
	
	public static String amountForm()
	{
		return "<form method=\"post\" action=\"/\" class=\"form\">\n" +
"    <div class=\"form-item\">\n" +
"        <label>Ammontare lordo</label>\n" +
"        <input type=\"text\" name=\"amount\" class=\"w50\">\n" +
"    </div>\n" +
"    <div class=\"form-item\">\n" +
"        <button>Calcola</button>\n" +
"    </div>\n" +
"</form>";

	}
	/**
	 * 
	 * @param url
	 * @return 
	 */
	public static String js(String url)
	{
		return "<script src=\""+url+"\"></script>";
	}
	
	/**
	 * 
	 * @param url
	 * @return 
	 */
	public static String css(String url)
	{
		return "<link rel=\"stylesheet\" href=\""+url+"\">";
	}

	/**
	 * 
	 * @param content
	 * @return
	 */
	public static String layout(String content)
	{
		return layout("Quanto paghi in tasse e contributi con una partita IVA?", content);
	}
	
	
	public static String renderCalculator(double amount, String mode)
	{
		String content = "";
	
				
		
		Calculator c = Calculator.make(mode);
		
		content += "<h3>"+c.getLabel()+"</h3>";
		content += "<p>"+c.getDescription()+"</p>";
		
		c.setGrossAmount(amount);
		c.calculate();
		String error = c.getError();
		if(error == null){
			content += "<ul class=\"unstyled\">";
			List<TaxEntry> es = c.getEntries();
			for(TaxEntry e : es){
				content += "<li>"+e.naiveRender()+"</li>";

			}
			double taxes = c.getTotalOnEntries();
			content += "<li><strong>Lordo: "+formatDecimal(amount)+"</strong></li>";
			content += "<li><strong>Tasse: "+formatDecimal(taxes)+"</strong></li>";
			content += "<li><strong>Ti rimane: "+formatDecimal(amount - taxes)+"</strong></li>";
			content += "<li><strong>Cioè circa: "+formatDecimal((amount - taxes) / 12)+" al mese</strong></li>";

			content += "</ul>";
		}
		else {
			content += "<p style=\"color:red\">"+error+"</p>";
		}
		return row(col(content, 12));
	}
	
	public static String row(String content)
	{
		return "<div class=\"row\">"+content+"</div>";

	}
	
	public static String col(String content, int size)
	{
		return "<div class=\"col col-"+size+"\">"+content+"</div>";

	}
	
	public static String formatDecimal(double number) {
		double epsilon = 0.004; // 4 tenths of a cent
		if (Math.abs(Math.round(number) - number) < epsilon) {
			return String.format("%10.0f", number); // sdb
		} else {
			return String.format("%10.2f", number); // dj_segfault
		}
	}
}