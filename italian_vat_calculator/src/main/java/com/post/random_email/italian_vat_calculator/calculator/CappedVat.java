/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.italian_vat_calculator.calculator;

/**
 *
 * @author salvix
 */
public class CappedVat extends Calculator {
	
	private final int rateINPS = 4;
	
	private final float rateINPSamount = 650;
	
	private final double percentualSostitutiva = 67;
	
	private final double impostaSostitutiva = 15;
	
	private final double sogliaINPS = 15000;
	
	private final double percentualeINPS = 24;
	
	private double baseImponibile; 
	
	private final double cap = 64999.99;
	
	@Override
	public void calculate() {
		if(grossAmount < cap){
			baseImponibile = (percentualSostitutiva * grossAmount) / 100 ;

			for(int i = 0; i < rateINPS; i++){
				TaxEntry te = new TaxEntry();
				te.amount = rateINPSamount;
				te.description = "Rata #"+(i+1)+" INPS fissa (Mag/Ago/Nov/Feb)";
				addEntry(te);
			}

			TaxEntry impSostitutiva = new TaxEntry();
			impSostitutiva.description = "Imposta sostitutiva al 15% (primi 5 anni invece agevolato al 5%)";
			impSostitutiva.amount = (this.impostaSostitutiva * baseImponibile) / 100;

			addEntry(impSostitutiva);

			TaxEntry inps = new TaxEntry();
			double baseInps = java.lang.Math.max(0, baseImponibile - sogliaINPS);

			inps.description = "Prelievo INPS 24 % sopra soglia "+sogliaINPS + " fino a base Imponibile "+baseImponibile;
			inps.amount = (baseInps) * percentualeINPS / 100;

			addEntry(inps);
		}
		else {
			setError("Hai ecceduto il limite di "+formatDecimal(cap)+"!");		
		}
	}

	@Override
	public String getLabel() {
		return "Regime Forfettario";
	}
	
	@Override
	public String getDescription() {
		return "";
	}

	
}
