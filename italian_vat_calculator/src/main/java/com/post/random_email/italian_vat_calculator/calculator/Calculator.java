/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.italian_vat_calculator.calculator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author salvix
 */
public abstract class Calculator {
	
	protected double grossAmount;
	
	private String mode;
	
	private Calculator actualCalculator;
	
	private List<TaxEntry> entries;
	
	private String errorMessage = null;
	
	
	public Calculator()
	{
		entries = new ArrayList<>();
	}
	
	public void addEntry(TaxEntry e)
	{
		entries.add(e);
	}
	
	public void addEntries(List<TaxEntry> es)
	{
		for(TaxEntry e: es){
			entries.add(e);
		}
	}
	
	public static String formatDecimal(double number) {
		 return String.format("%10.2f", number);
	}
	
	public void setError(String error)
	{
		errorMessage = error;
	}
	
	public String getError()
	{
		return errorMessage;
	}
	
	public static Calculator make(String mode)
	{
		Calculator c;
		if(mode.equals("standard")){
			c = new StandardVat();
		}
		else {
			c = new CappedVat();
		}
		c.setMode(mode);
		return c;
	}
	
	public void setGrossAmount(double grossAmount)
	{
		this.grossAmount = grossAmount;
	}
	
	public void setMode(String mode)
	{
		this.mode = mode;
	}
	
	
	public List<TaxEntry> getEntries()
	{
		return entries;
	}
	
	public double getTotalOnEntries()
	{
		double total = 0.0;
		for(TaxEntry e : entries){
			total += e.amount;
		}
		return total;
	}
	
	
	public String title()
	{
		return this.mode;
	}
	

	abstract public String getDescription();
	
	abstract public void calculate();
	
	abstract public String getLabel();
}
