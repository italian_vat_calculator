/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.italian_vat_calculator.calculator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author salvix
 */
public class StandardVat extends Calculator {
	
	private final int rateINPS = 4;
	
	private final float rateINPSamount = 900;
	
	private final double percentualSostitutiva = 100;
	
	
	private final double sogliaINPS = 15000;
	
	private final double percentualeINPS = 24;
	
	private final double percentualeIRAP = 3.9;
	
	private double baseImponibile; 
	
	static class Aliquota {
		private double left = 0;
		private double right = 0;
		private double percentage = 0;
		
		public Aliquota(double left, double right, double percentage){
			this.left = left;
			this.right = right;
			this.percentage = percentage;
		}
		
		public boolean isAmountInRange(double amount){
			return (amount > left);
		}
		
		public double inRangeBy(double amount)
		{
			if(isAmountInRange(amount)){
				
				double exceedingAmount = amount - left;
				return java.lang.Math.min(exceedingAmount, right - left);
			}
			return 0.0;
		}
		
		
		public double calculate(double amount)
		{
			double actualAmount = inRangeBy(amount);
			return (actualAmount * percentage) / 100;
		}
		
	}
	
	
	public static List<Aliquota> aliquoteIrpef()
	{
		List<Aliquota> aliquoteIrpef = new ArrayList<Aliquota>();
		aliquoteIrpef.add(new Aliquota(0, 15000, 23));
		aliquoteIrpef.add(new Aliquota(15000, 28000, 27));
		aliquoteIrpef.add(new Aliquota(28000, 55000, 38));
		aliquoteIrpef.add(new Aliquota(55000, 75000, 41));
		aliquoteIrpef.add(new Aliquota(75000, 1000000, 43));
		return aliquoteIrpef;
	}
	
	@Override
	public void calculate() {
		
		baseImponibile = (percentualSostitutiva * grossAmount) / 100 ;
		List<TaxEntry> contributi = inps(baseImponibile);
		double contributiInps = 0;
		for(TaxEntry te : contributi){
			contributiInps += te.amount;
		}
		List<Aliquota> aliquoteIrpef = aliquoteIrpef();
		
		double baseIrpef = baseImponibile - contributiInps;
		
		addEntries(contributi);
		addEntries(irpef(aliquoteIrpef, baseIrpef));
//		addEntries();
		addEntry(irap(baseImponibile));
	}
	
	
	public List<TaxEntry> irpef(List<Aliquota> aliquoteIrpef, double amount)
	{
		List<TaxEntry> irpef = new ArrayList<TaxEntry>();
		for(Aliquota a : aliquoteIrpef){
			if(a.isAmountInRange(amount)){
				double inRange = a.inRangeBy(amount);
				TaxEntry te = new TaxEntry();
				te.amount = a.calculate(amount);
				te.description = formatDecimal(inRange) + " @ "+a.percentage+" %";
				irpef.add(te);
			}
			else {
				break;
			}
		
		}
		return irpef;
	}
	
	public List<TaxEntry> inps(double amount)
	{
		List<TaxEntry> inps = new ArrayList<TaxEntry>();
		
		for(int i = 0; i < rateINPS; i++){
			TaxEntry te = new TaxEntry();
			te.amount = rateINPSamount;
			te.description = "Rata #"+(i+1)+" INPS fissa";
			inps.add(te);
		}
		
		TaxEntry inpsSoglia = new TaxEntry();
		double baseInps = java.lang.Math.max(0, amount - sogliaINPS);
		inpsSoglia.description = "Prelievo INPS 24 % sopra soglia "+formatDecimal(sogliaINPS) + " fino a base imponibile "+formatDecimal(amount);
		inpsSoglia.amount = (baseInps) * percentualeINPS / 100;
		
		inps.add(inpsSoglia);
		return inps;
	}
	
	public TaxEntry irap(double amount)
	{
		TaxEntry irap = new TaxEntry();
		
		irap.description = "Prelievo IRAP 3.9 % su "+formatDecimal(amount);
		irap.amount = (amount) * percentualeIRAP / 100;
		return irap;
	}
	
	@Override
	public String getLabel() {
		return "Partita IVA";
	}
	
	@Override
	public String getDescription() {
		return "";
	}
	
}
