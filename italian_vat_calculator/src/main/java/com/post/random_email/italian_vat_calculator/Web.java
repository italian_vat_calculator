package com.post.random_email.italian_vat_calculator;

import static spark.Spark.*;
import static com.post.random_email.italian_vat_calculator.View.*;
import com.post.random_email.italian_vat_calculator.calculator.Calculator;
import com.post.random_email.italian_vat_calculator.calculator.TaxEntry;
import java.util.List;

public class Web {
	
	public static void init(){
		
		get("/", (request, response) -> {
    		String amountForm = row(col(amountForm(), 12));
			String content = amountForm;
    		return layout(content);
		});
		
		get("/:amount", (request, response) -> {
    		String amountForm = row(col(amountForm(), 12));
			String amountParam = request.params(":amount");
//			if(request.)
			String content = amountForm;
			double amount = Double.parseDouble(amountParam);
			content += renderCalculator(amount, "capped");
			content += renderCalculator(amount, "standard");
    		return layout(content);
		});
		
		post("/", (request, response) -> {
			// Create something
			String amountParam = request.queryParams("amount");
			response.redirect("/"+amountParam);
            return null;
		});
	}
}