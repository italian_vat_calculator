/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.italian_vat_calculator.calculator;

import com.post.random_email.italian_vat_calculator.calculator.TaxEntry;
import com.post.random_email.italian_vat_calculator.calculator.Calculator;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author salvix
 */
public class CalculatorTest {
	
	public CalculatorTest() {
	}

	@org.junit.Test
	public void testSomeMethod() {
		
		double sales = 30000;
		Calculator c = Calculator.make("capped");
		c.setGrossAmount(sales);
		c.calculate();
		List<TaxEntry> es = c.getEntries();
		for(TaxEntry e : es){
			System.out.println(e.naiveRender());
		}
		double taxes = c.getTotalOnEntries();
		
		System.out.println("Taxes "+(taxes));
		System.out.println("You are left with "+(sales - taxes));
		assertEquals(4629.0, taxes, 0.01);
		
	}
	
	
	@org.junit.Test
	public void testSomeOtherMethod() {
		
		double sales = 40000;
		Calculator c = Calculator.make("standard");
		c.setGrossAmount(sales);
		c.calculate();
		List<TaxEntry> es = c.getEntries();
		for(TaxEntry e : es){
			System.out.println(e.naiveRender());
		}
		double taxes = c.getTotalOnEntries();
		
		System.out.println("Taxes "+(taxes));
		System.out.println("You are left with "+(sales - taxes));
		assertEquals(19032.0, taxes, 0.01);
		
	}
	
}
